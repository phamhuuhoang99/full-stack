import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

import firstPage from "./components/pages/myFirstVuePage";
import newRoutePage from "./components/pages/newRoute";
import hooks from "./components/pages/basic/hooks.vue";
import methods from "./components/pages/basic/methods.vue";

//admin project pages
import home from "./components/pages/home.vue";
import tags from "./admin/pages/tags.vue";
import category from "./admin/pages/category.vue";
import adminusers from "./admin/pages/adminusers.vue";
import login from "./admin/pages/login.vue";
import role from "./admin/pages/role";
import assignRole from "./admin/pages/assignRole";
import createBlog from "./admin/pages/createBlog";

import usercom from "./vuex/usecom.vue";

const routes = [
    //project router

    {
        path: "/",
        component: home,
        name: "/"
    },
    {
        path: "/login",
        component: login,
        name: "login"
    },
    {
        path: "/tags",
        component: tags,
        name: "tags"
    },
    {
        path: "/category",
        component: category,
        name: "category"
    },
    {
        path: "/createBlog",
        component: createBlog,
        name: "createBlog"
    },
    {
        path: "/adminusers",
        component: adminusers,
        name: "adminusers"
    },
    {
        path: "/role",
        component: role
    },
    {
        path: "/assignRole",
        component: assignRole,
        name: "assignRole"
    },
    //test
    {
        path: "/testvuex",
        component: usercom
    },
    {
        path: "/my-new-vue-route",
        component: firstPage
    },
    {
        path: "/new-route",
        component: newRoutePage
    },

    //vue hooks
    {
        path: "/hooks",
        component: hooks
    },
    //vue hooks
    {
        path: "/methods",
        component: methods
    }
];

export default new Router({
    mode: "history",
    routes
});